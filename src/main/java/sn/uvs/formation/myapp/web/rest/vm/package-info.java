/**
 * View Models used by Spring MVC REST controllers.
 */
package sn.uvs.formation.myapp.web.rest.vm;
