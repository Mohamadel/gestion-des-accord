package sn.uvs.formation.myapp;

import com.tngtech.archunit.core.domain.JavaClasses;
import com.tngtech.archunit.core.importer.ClassFileImporter;
import com.tngtech.archunit.core.importer.ImportOption;
import org.junit.jupiter.api.Test;

import static com.tngtech.archunit.lang.syntax.ArchRuleDefinition.noClasses;

class ArchTest {

    @Test
    void servicesAndRepositoriesShouldNotDependOnWebLayer() {

        JavaClasses importedClasses = new ClassFileImporter()
            .withImportOption(ImportOption.Predefined.DO_NOT_INCLUDE_TESTS)
            .importPackages("sn.uvs.formation.myapp");

        noClasses()
            .that()
                .resideInAnyPackage("sn.uvs.formation.myapp.service..")
            .or()
                .resideInAnyPackage("sn.uvs.formation.myapp.repository..")
            .should().dependOnClassesThat()
                .resideInAnyPackage("..sn.uvs.formation.myapp.web..")
        .because("Services and repositories should not depend on web layer")
        .check(importedClasses);
    }
}
